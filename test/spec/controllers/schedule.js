'use strict';

describe('Controller: ScheduleCtrl', function () {

  // load the controller's module
  beforeEach(module('IonicApp'));

  var ScheduleCtrl, scope, stateParams;
  var ApiService = {
       get: function() {
         return { "items": [{"startTimeSeconds" : "1405648800","startTime" : "7:00 pm","endTime" : "8:10 pm","classID" : "1" ,"classType" : "Green" ,"location" : "Fullerton","locationID" : "1","firstName" : "Angie","lastName" : "Su","instructor" : "Angie Su","staffID" : "100000154"}]};
       }
     }

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, $injector) {
    scope = $rootScope.$new();
    stateParams = { id: 1 };

    ScheduleCtrl = $controller('ScheduleCtrl', {
      $scope: scope,
      $stateParams: stateParams,
      ApiService : ApiService
    });
  }));


  it('should be Fullerton', function() {
    expect(scope.locationName).to.equal('Fullerton');
  });

  it('should be an array', function(){
    var data = ApiService.get().items;

    expect(data).to.be.a('array');
  });


});
