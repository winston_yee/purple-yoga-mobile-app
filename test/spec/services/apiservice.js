'use strict';

describe('Service: Apiservice', function () {

  // load the service's module
  beforeEach(module('ionicApp'));

  // instantiate service
  var Apiservice;
  beforeEach(inject(function (_Apiservice_) {
    Apiservice = _Apiservice_;
  }));

  it('should do something', function () {
    expect(!!Apiservice).toBe(true);
  });

});
