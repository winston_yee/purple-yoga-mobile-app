'use strict';

/**
 * @ngdoc function
 * @name ionicApp.controller:ScheduleCtrl
 * @description
 * # ScheduleCtrl
 * Controller of the ionicApp
 */
angular.module('IonicApp')
    .controller('ScheduleCtrl', function ($scope, $stateParams, ApiService, LocationService) {
        var locId = $stateParams.id;

        //Dev code
        // var results         = JSON.parse( ApiService.get() );
        // $scope.results      = results.items;

        var locations       = LocationService.getAll();

        $scope.locations    = locations;
        $scope.locationName = LocationService.get( locId )[0].name;

        // Do no delete prod code
        ApiService.get( locId )
            .success( function(data){
                $scope.results = data.items;
            });

});
