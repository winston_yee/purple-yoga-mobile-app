'use strict';

/**
 * @ngdoc function
 * @name ionicAppApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the ionicAppApp
 */
angular.module('IonicApp')
  .controller('HomeCtrl', function ($scope) {
    $scope.menuItems = [
      { id:'featuredEvents', href: '#/app/search',            text:'Featured Events' },
      { id:'fulSched',       href: '#/app/schedule/1',      text:'Fullerton Schedule' },
      { id:'longSched',      href: '#/app/schedule/2',     text:'Long Beach Schedule' },
      { id:'tusSched',       href: '#/app/schedule/3',      text:'Tustin Schedule' },
      { id:'browseClass',    href: '#/app/searchbrowseClass', text:'Browse by Class' },
      { id:'instructors',    href: '#/app/instructors',       text:'Instructors' }
    ];

  });
