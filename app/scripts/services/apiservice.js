'use strict';

/**
 * @ngdoc service
 * @name ionicApp.Apiservice
 * @description
 * # Apiservice
 * Service in the ionicApp.
 */
angular.module('IonicApp')
  .service('ApiService', function ApiService($http, LocationService) {
    //   var result = '{ "items": [{"startTimeSeconds" : "1403365500","startTime" : "8:45 am","endTime" : "9:55 am","classID" : "1" ,"classType" : "Green" ,"location" : "Fullerton","locationID" : "1","firstName" : "Janna","lastName" : "Colaco","instructor" : "Janna Colaco","staffID" : "100000007"},{"startTimeSeconds" : "1403390700","startTime" : "3:45 pm","endTime" : "4:55 pm","classID" : "1" ,"classType" : "Green" ,"location" : "Fullerton","locationID" : "1","firstName" : "Jennifer","lastName" : "Harvey","instructor" : "Jennifer Harvey","staffID" : "100000121"},{"startTimeSeconds" : "1403371800","startTime" : "10:30 am","endTime" : "11:40 am","classID" : "10" ,"classType" : "Red" ,"location" : "Fullerton","locationID" : "1","firstName" : "Bob","lastName" : "DeRobbio","instructor" : "Bob DeRobbio","staffID" : "100000274"},{"startTimeSeconds" : "1403397000","startTime" : "5:30 pm","endTime" : "6:30 pm","classID" : "13" ,"classType" : "$9 - Blue" ,"location" : "Fullerton","locationID" : "1","firstName" : "Jennifer","lastName" : "Harvey","instructor" : "Jennifer Harvey","staffID" : "100000121"},{"startTimeSeconds" : "1403359200","startTime" : "7:00 am","endTime" : "8:10 am","classID" : "14" ,"classType" : "$9 - Red" ,"location" : "Fullerton","locationID" : "1","firstName" : "Janna","lastName" : "Colaco","instructor" : "Janna Colaco","staffID" : "100000007"},{"startTimeSeconds" : "1403384400","startTime" : "2:00 pm","endTime" : "3:10 pm","classID" : "14" ,"classType" : "$9 - Red" ,"location" : "Fullerton","locationID" : "1","firstName" : "Jennifer","lastName" : "Harvey","instructor" : "Jennifer Harvey","staffID" : "100000121"},{"startTimeSeconds" : "1403378100","startTime" : "12:15 pm","endTime" : "1:35 pm","classID" : "54" ,"classType" : "Green 2.0" ,"location" : "Fullerton","locationID" : "1","firstName" : "Jennifer","lastName" : "Harvey","instructor" : "Jennifer Harvey","staffID" : "100000121"},{"startTimeSeconds" : "1403402400","startTime" : "7:00 pm","endTime" : "9:00 pm","classID" : "207" ,"classType" : "Blindfolds & Blocks: Samurai Sensory School (Fullerton)" ,"location" : "Fullerton","locationID" : "1","firstName" : "Cidney (Shidoni)","lastName" : "Corpuz","instructor" : "Cidney (Shidoni) Corpuz","staffID" : "100000231"}]}';

        // Dev purposes

        // var getInfo = function(){
        //     return result;
        // };

        //Prod
        var getScheduleById = function( locId ){
            var url = createURL( locId );
            return $http.get( url, {cache: true} );
        };

        var createURL = function( locId ){
            var url = 'http://daily.purpleyoga.org/services/classService.php?LocationIDs=';
            url     = url + LocationService.get( locId )[0].id;
            return url;
        };

        return{
            get: getScheduleById
        };



  });
