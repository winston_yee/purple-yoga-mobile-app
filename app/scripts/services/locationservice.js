'use strict';

/**
 * @ngdoc service
 * @name ionicApp.LocationService
 * @description
 * # LocationService
 * Service in the ionicApp.
 */

angular.module('IonicApp')
  .service('LocationService', function LocationService() {

        var getLocations = function(){
            return [
                    { id : 1, name: 'Fullerton' },
                    { id : 2, name: 'Long Beach' },
                    { id : 3, name: 'Tustin'}
            ];
        };

        var getLocationById = function( id ){
            var locations   = getLocations();

            var location = locations.filter( function(location){
                                return location.id === +id;
                            });

            return location;
        };

        return {    getAll : getLocations,
                    get    : getLocationById
        };

  });
